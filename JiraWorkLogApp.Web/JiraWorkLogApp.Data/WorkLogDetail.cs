﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JiraWorkLogApp.Data
{
    public class WorkLogDetail
    {
        public int Id { get; set; }
        public string SprintName { get; set; }
        public string IssueId { get; set; }
        public string IssueType { get; set; }
        public string Summary { get; set; }
        public string Author { get; set; }
        public string UserId { get; set; }
        public DateTime? LoggedDate { get; set; }
        public long TimeSpent { get; set; }
        public string Comment { get; set; }
        public bool Billable { get; set; }
        public decimal LoggedTime { get; set; }
    }
}
