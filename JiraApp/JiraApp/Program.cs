﻿using Atlassian.Jira;
using JiraApp.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace JiraApp
{
    class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {

            try
            {
                var jiraUrl = ConfigurationManager.AppSettings["JiraUrl"];
                var jiraUser = ConfigurationManager.AppSettings["JiraUser"];
                var fromTime = Convert.ToDateTime(ConfigurationManager.AppSettings["FromTime"]);

                var workLogRepository = new WorkLogRepository();
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                var jira = Jira.CreateRestClient(jiraUrl, jiraUser, "pV3DD3UajOwdxSc695oXAEA1");

                workLogRepository.RemoveWorkLogs();
                var sprintList = workLogRepository.GetSprintList();

                foreach (var sprint in sprintList)
                {
                    //Console.WriteLine("Starting Sprint " + sprint.Name);
                    log.Info("Starting Sprint " + sprint.Name);

                    var totalIssues = 0;
                    var returnIssues = 0;
                    var startAt = 0;

                    do
                    {
                        var jql = string.Format("project = AUT AND sprint = {0}", sprint.JiraId);
                        var issues = jira.Issues.GetIssuesFromJqlAsync(jql, maxIssues: 5000, startAt: startAt).Result;
                        totalIssues = issues.TotalItems;
                        returnIssues += issues.Count();
                        startAt = returnIssues;

                        //long totalTime = 0;
                        foreach (var issue in issues)
                        {

                            var jiraWorkLogs = from w in issue.GetWorklogsAsync().Result
                                               where w.StartDate > fromTime
                                               select w;

                            foreach (var jwl in jiraWorkLogs)
                            {
                                var workLog = new WorkLog();
                                workLog.SprintId = sprint.Id;
                                workLog.IssueId = issue.Key.Value;
                                workLog.IssueType = issue.Type.Name;
                                workLog.Summary = issue.Summary;
                                workLog.Author = jwl.Author;
                                workLog.TimeSpent = jwl.TimeSpentInSeconds;
                                workLog.LoggedDate = jwl.StartDate;
                                workLog.Comment = jwl.Comment;

                                workLogRepository.AddWorkLog(workLog);

                            }

                            //totalTime += jiraWorkLogss.Select(x => x.TimeSpentInSeconds).Sum();

                        }




                    } while (returnIssues < totalIssues);

                    var status = workLogRepository.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex);
                log.Error(ex);
            }
            //Console.WriteLine("Completed");
            //Console.WriteLine("---------------------------------------------------------------");
            log.Info("Completed");
            log.Info("---------------------------------------------------------------");
        }

    }
}
