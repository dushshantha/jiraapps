﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JiraApp.Data
{
    public class WorkLogRepository
    {
        private readonly JiraWorkLogContext _context;

        public WorkLogRepository()
        {
            _context = new JiraWorkLogContext();
        }

        public IList<Sprint> GetSprintList()
        {
            return _context.Sprints.Where(s => s.IsActive).ToList();
        }

        public void AddWorkLog(WorkLog workLog)
        {
            _context.WorkLogs.Add(workLog);
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        public void RemoveWorkLogs()
        {
            _context.Database.ExecuteSqlCommand("exec RemoveAllWorkLogs");
        }
    }
}
