namespace JiraApp.Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class JiraWorkLogContext : DbContext
    {
        public JiraWorkLogContext()
            : base("name=JiraWorkLogs")
        {
        }

        public virtual DbSet<Sprint> Sprints { get; set; }
        public virtual DbSet<WorkLog> WorkLogs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
