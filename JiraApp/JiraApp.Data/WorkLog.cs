﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JiraApp.Data
{
    public class WorkLog
    {
        public int Id { get; set; }
        public int SprintId { get; set; }
        public string IssueId { get; set; }
        public string IssueType { get; set; }
        public string Summary { get; set; }
        public string Author { get; set; }
        public long TimeSpent { get; set; }
        public DateTime? LoggedDate { get; set; }
        public string Comment { get; set; }
    }
}
