namespace JiraWorkLogApp.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class WorkLog
    {
        public int Id { get; set; }

        public int SprintId { get; set; }

        [Required]
        [StringLength(20)]
        public string IssueId { get; set; }

        [Required]
        [StringLength(20)]
        public string IssueType { get; set; }

        public string Summary { get; set; }

        [Required]
        [StringLength(50)]
        public string Author { get; set; }

        public long TimeSpent { get; set; }

        public DateTime? LoggedDate { get; set; }

        public string Comment { get; set; }

        public bool Billable { get; set; }
    }
}
