﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JiraWorkLogApp.Web.ViewModels
{
    public class DailyWorkLog
    {
        public string Date { get; set; }
        public decimal Time { get; set; }
    }
}