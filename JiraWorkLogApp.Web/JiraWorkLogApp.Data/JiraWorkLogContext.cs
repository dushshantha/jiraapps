namespace JiraWorkLogApp.Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class JiraWorkLogContext : DbContext
    {
        public JiraWorkLogContext()
            : base("name=JiraWorkLogContext")
        {
        }

        public virtual DbSet<Sprint> Sprints { get; set; }
        public virtual DbSet<WorkLog> WorkLogs { get; set; }
        public virtual DbSet<WorkLogDetail> WorkLogDetails { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<WorkLog>()
                .Property(e => e.IssueId)
                .IsUnicode(false);

            modelBuilder.Entity<WorkLog>()
                .Property(e => e.IssueType)
                .IsUnicode(false);
        }
    }
}
