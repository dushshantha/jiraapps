﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JiraWorkLogApp.Web.ViewModels
{
    public class SummaryWorkLog
    {
        public string SprintName { get; set; }
        public string IssueId { get; set; }
        public string Summary { get; set; }
        public string LoggedDate { get; set; }
        public decimal LoggedTime { get; set; }
    }
}