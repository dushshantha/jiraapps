﻿using JiraWorkLogApp.Data;
using JiraWorkLogApp.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JiraWorkLogApp.Web.Controllers
{
    [Authorize]
    public class WorkLogController : Controller
    {
        private readonly WorkLogRepository _workLogRepository;
        public WorkLogController()
        {
            _workLogRepository = new WorkLogRepository();
        }

        // GET: WorkLog
        public ActionResult Daily()
        {
            var userId = User.Identity.Name;
            var allLogs = _workLogRepository.GetAllWorkLogs();
            var dailyList = from wl in allLogs
                            where wl.UserId == userId
                            group wl by wl.LoggedDate into grp
                            select grp;

            var workLogList = new List<DailyWorkLog>();

            foreach (var item in dailyList)
            {
                var wl = new DailyWorkLog
                {
                    Date = item.Key.Value.ToShortDateString(),
                    Time = item.Sum(x => x.LoggedTime)
                };
                workLogList.Add(wl);
            }

            return View(workLogList);
        }

        public ActionResult Summary()
        {
            var userId = User.Identity.Name;
            var allLogs = _workLogRepository.GetAllWorkLogs();
            var summaryList = from wl in allLogs
                              where wl.UserId == userId
                              select wl;

            var workLogList = new List<SummaryWorkLog>();
            foreach (var item in summaryList.OrderBy(x=>x.LoggedDate))
            {
                var wl = new SummaryWorkLog
                {
                    SprintName = item.SprintName,
                    IssueId = item.IssueId,
                    Summary = item.Summary,
                    LoggedDate = item.LoggedDate.Value.ToShortDateString(),
                    LoggedTime = item.LoggedTime
                };
                workLogList.Add(wl);
            }

            return View(workLogList);
        }

        public ActionResult Team()
        {
            return View();
        }

    }
}