﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JiraWorkLogApp.Data
{
    public class WorkLogRepository
    {
        private readonly JiraWorkLogContext _jiraContext;

        public WorkLogRepository()
        {
            _jiraContext = new JiraWorkLogContext();
        }

        public IQueryable<WorkLogDetail> GetAllWorkLogs()
        {
            return _jiraContext.WorkLogDetails;
        }
    }
}
