namespace JiraApp.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Sprint
    {
        public int Id { get; set; }

        public int JiraId { get; set; }

        [StringLength(50)]
        public string Name { get; set; }
        public bool IsActive { get; set; }  
    }
}
